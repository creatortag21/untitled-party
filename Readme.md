### Prerequisites

You will need: 

* [Blender](https://www.blender.org/) - or any software
* [Epic games](https://www.epicgames.com/) - Epic games account/launcher for launching unreal engine
* [Unreal Engine](https://www.unrealengine.com/en-US/get-now) - Unreal engine to compile the game
* [GitKraken](https://www.gitkraken.com/) - Git management tool for committing/pulling/... from bitbucket repositories

## Getting Started

* Access the project you want to create a git repository to on bitbucket.org.
* Select the projet you want to download on your local machine
* In overview you should be able to find the url it should look like http://@yourname/project/something/something copy it
* Open git kraken
* Press File > Clone repo
* By default it should select 'Clone with URL' you can leave it there
* Select a folder on your machine and paste the url
* Click "Clone the repo!"
* You should have now pulled all the source code, fking hooray

### Typical creation of a game asset

* Create a high-poly mesh of the asset you want including all the details (no more than 1kk poly if possible, include scars on the skin, wrinkles, cracks on a wall, and so on)
* Create a low-poly version of the mesh to use in the game, 5-7k polygons per asset it's ok depeding what you are making, obviously a box doesn't need that many polygons
* Create uv map
* Bake the character, make a nomal map and an ambient occlusion map
* Texture paint
* Animate

## Versioning

If you fked up something really bad on you machine open Git Kraken, select one of the commits from list of commits, right click and select "Revert master to this commit" and select the option you want from there

## Authors

* **Mario** - *Initial work* - Aka Professor Bum
* **Matt** - *Initial work* - Aka The Bearded Matt

## License

We are fking open to hacks yo something has to be done aye

## Acknowledgments

* Hat tip to anyone who's code was used
